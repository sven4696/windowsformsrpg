﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppCore
{
    public partial class BeastForm : Form
    {
        public BeastForm()
        {
            InitializeComponent();
        }

        // Redirects to the previous page whith all characters displayed
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListAllCharacters f2 = new ListAllCharacters();
            f2.ShowDialog();
        }

        // Method that deletes the selected beast from the database
        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sql = "DELETE FROM Beast WHERE ID = @beastID";
                    string beastID = listBox1.SelectedItem.ToString().Split(" ")[0];

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@beastID", beastID);

                        command.ExecuteNonQuery();
                    }
                    listBox1.Items.Clear();
                    BeastForm_Load(sender, e);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that laods all beasts to display them in a listbox
        private void BeastForm_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sqlWizard = "SELECT * FROM Beast";

                    using (SqlCommand command = new SqlCommand(sqlWizard, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            listBox1.Items.Add($"{reader.GetName(0)} \t{reader.GetName(1)} \t {reader.GetName(2)} \t {reader.GetName(3)} \t {reader.GetName(4)} \t {reader.GetName(5)} \t {reader.GetName(6)}");

                            while (reader.Read())
                            {
                                listBox1.Items.Add($"{reader.GetInt32(0)} \t {reader.GetString(1)} \t {reader.GetString(2)} \t {reader.GetInt32(3)} \t {reader.GetInt32(4)} \t {reader.GetInt32(5)} \t {reader.GetInt32(5)}");
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that updates the selected changes and stores them in a database
        private void button2_Click(object sender, EventArgs e)
        {
            string url = "UPDATE Beast Set Name = @name, HP = @hp, Mana = @mana, ArmorRating = @armorRating, RageMode = @rage Where ID = @beastID";

            string beastID = listBox1.SelectedItem.ToString().Split(" ")[0];
            string name = txtName.Text;
            int hp = int.Parse(txtHp.Text);
            int mana = int.Parse(txtMana.Text);
            int armorRating = int.Parse(txtArmor.Text);
            string rage = txtSpecial.Text;

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(url, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@hp", hp);
                        command.Parameters.AddWithValue("@mana", mana);
                        command.Parameters.AddWithValue("@armorRating", armorRating);
                        command.Parameters.AddWithValue("@rage", rage);
                        command.Parameters.AddWithValue("@beastID", beastID);

                        command.ExecuteNonQuery();

                    }
                    listBox1.Items.Clear();
                    BeastForm_Load(sender, e);
                    txtName.Clear();
                    txtHp.Clear();
                    txtMana.Clear();
                    txtArmor.Clear();
                    txtSpecial.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that brings the selected attributes and stores them in textboses for the user to change
        private void button3_Click(object sender, EventArgs e)
        {
            string name = listBox1.SelectedItem.ToString().Split(" ")[4];
            txtName.Text = name;

            string BeastHP = listBox1.SelectedItem.ToString().Split(" ")[6];
            txtHp.Text = BeastHP;

            string BeastMana = listBox1.SelectedItem.ToString().Split(" ")[8];
            txtMana.Text = BeastMana;

            string BeastArmor = listBox1.SelectedItem.ToString().Split(" ")[10];
            txtArmor.Text = BeastArmor;

            string BeastSpecial = listBox1.SelectedItem.ToString().Split(" ")[12];

            txtSpecial.Text = BeastSpecial ;
        }
    }
}
