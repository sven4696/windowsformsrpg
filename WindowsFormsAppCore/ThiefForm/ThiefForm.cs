﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppCore
{
    public partial class ThiefForm : Form
    {
        public ThiefForm()
        {
            InitializeComponent();
        }

        // Method that redirects to the previous form with all characters listed
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListAllCharacters f2 = new ListAllCharacters();
            f2.ShowDialog();
        }

        // Method which loads all thief characters from the database and displays them on screen
        private void ThiefForm_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sqlWizard = "SELECT * FROM Thief";

                    using (SqlCommand command = new SqlCommand(sqlWizard, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            listBox1.Items.Add($"{reader.GetName(0)} \t{reader.GetName(1)} \t {reader.GetName(2)} \t {reader.GetName(3)} \t {reader.GetName(4)} \t {reader.GetName(5)} \t {reader.GetName(6)}");

                            while (reader.Read())
                            {
                                listBox1.Items.Add($"{reader.GetInt32(0)} \t {reader.GetString(1)} \t \t {reader.GetString(2)} \t {reader.GetInt32(3)} \t {reader.GetInt32(4)} \t \t {reader.GetInt32(5)} \t {reader.GetDouble(6)}");

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that deletes the selected thief from the database
        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sql = "DELETE FROM Thief WHERE ID = @thiefID";
                    string thiefID = listBox1.SelectedItem.ToString().Split(" ")[0];

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@thiefID", thiefID);

                        command.ExecuteNonQuery();
                    }
                    listBox1.Items.Clear();
                    ThiefForm_Load(sender, e);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that brings the selected thief attributes and lists them in textboxes for the user to change
        private void button1_Click(object sender, EventArgs e)
        {
            string name = listBox1.SelectedItem.ToString().Split(" ")[5];
            txtName.Text = name;

            string thiefHP = listBox1.SelectedItem.ToString().Split(" ")[7];
            txtHP.Text = thiefHP;

            string thiefMana = listBox1.SelectedItem.ToString().Split(" ")[9];
            txtMana.Text = thiefMana;

            string thiefArmor = listBox1.SelectedItem.ToString().Split(" ")[12];
            txtArmor.Text = thiefArmor;

            string thiefSpecial = listBox1.SelectedItem.ToString().Split(" ")[14];
            txtSpecial.Text = thiefSpecial;
        }

        // Method that updates the selected changes and stores them in the database
        private void button2_Click(object sender, EventArgs e)
        {
            string url = "UPDATE Thief Set Name = @name, HP = @hp, Mana = @mana, ArmorRating = @armorRating, StealthMode = @stealth Where ID = @thiefID";

            string thiefID = listBox1.SelectedItem.ToString().Split(" ")[0];
            string name = txtName.Text;
            int hp = int.Parse(txtHP.Text);
            int mana = int.Parse(txtMana.Text);
            int armorRating = int.Parse(txtArmor.Text);
            double stealth = double.Parse(txtSpecial.Text);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(url, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@hp", hp);
                        command.Parameters.AddWithValue("@mana", mana);
                        command.Parameters.AddWithValue("@armorRating", armorRating);
                        command.Parameters.AddWithValue("@stealth", stealth);
                        command.Parameters.AddWithValue("@thiefID", thiefID);

                        command.ExecuteNonQuery();

                    }
                    listBox1.Items.Clear();
                    ThiefForm_Load(sender, e);

                    txtName.Clear();
                    txtHP.Clear();
                    txtMana.Clear();
                    txtArmor.Clear();
                    txtSpecial.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
