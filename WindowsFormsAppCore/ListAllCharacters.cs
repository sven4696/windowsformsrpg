﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppCore
{
    public partial class ListAllCharacters : Form
    {
        public ListAllCharacters()
        {
            InitializeComponent();
        }

        // Redirect towards creating character site
        private void btnThiefShow_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateCharacter f1 = new CreateCharacter();
            f1.ShowDialog();
        }

        // Redirect towards creating character site
        private void btnShowWizard_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateCharacter f1 = new CreateCharacter();
            f1.ShowDialog();
        }

        // Redirect towards creating character site
        private void btnShowBeasts_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateCharacter f1 = new CreateCharacter();
            f1.ShowDialog();
            
        }

        // Redirect towards creating character site
        private void btnShowWarrior_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateCharacter f1 = new CreateCharacter();
            f1.ShowDialog();
        }

        // Redirect to wizard form where one can edit and delete this character
        private void btnDelUpWizard_Click(object sender, EventArgs e)
        {
            this.Hide();
            WizardForm wf = new WizardForm();
            wf.ShowDialog();
        }

        // Redirect to beast form where one can edit and delete this character
        private void btnUpDelBeast_Click(object sender, EventArgs e)
        {
            this.Hide();
            BeastForm bf = new BeastForm();
            bf.ShowDialog();
        }

        // Redirect to thief form where one can edit and delete this 
        private void btnUpDelThief_Click(object sender, EventArgs e)
        {
            this.Hide();
            ThiefForm tf = new ThiefForm();
            tf.ShowDialog();
        }

        // Redirect to warrior form where one can edit and delete this character
        private void btnUpDelWarrior_Click(object sender, EventArgs e)
        {
            this.Hide();
            WarriorForm wf = new WarriorForm();
            wf.ShowDialog();
        }

        // Loading chracters from database when this form loads
        private void Form2_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sqlWarrior = "SELECT * FROM Warrior";

                    using (SqlCommand command = new SqlCommand(sqlWarrior, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lblWaT1.Text = reader.GetString(1);
                            lblWaN1.Text = reader.GetString(2);
                            lblWaH1.Text = reader.GetInt32(3).ToString();
                            lblWaM1.Text = reader.GetInt32(4).ToString();
                            lblWaA1.Text = reader.GetInt32(5).ToString();
                            lblWaS1.Text = reader.GetString(6);


                            reader.Read();
                            lblWaT2.Text = reader.GetString(1);
                            lblWaN2.Text = reader.GetString(2);
                            lblWaH2.Text = reader.GetInt32(3).ToString();
                            lblWaM2.Text = reader.GetInt32(4).ToString();
                            lblWaA2.Text = reader.GetInt32(5).ToString();
                            lblWaS2.Text = reader.GetString(6);


                            reader.Read();
                            lblWaT3.Text = reader.GetString(1);
                            lblWaN3.Text = reader.GetString(2);
                            lblWaH3.Text = reader.GetInt32(3).ToString();
                            lblWaM3.Text = reader.GetInt32(4).ToString();
                            lblWaA3.Text = reader.GetInt32(5).ToString();
                            lblWaS3.Text = reader.GetString(6);
                        }
                    }

                    string sqlWizard = "SELECT * FROM Wizard";

                    using (SqlCommand command = new SqlCommand(sqlWizard, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lblWiT1.Text = reader.GetString(1);
                            lblWiN1.Text = reader.GetString(2);
                            lblWiH1.Text = reader.GetInt32(3).ToString();
                            lblWiM1.Text = reader.GetInt32(4).ToString();
                            lblWiA1.Text = reader.GetInt32(5).ToString();
                            lblWiS1.Text = reader.GetString(6);


                            reader.Read();
                            lblWiT2.Text = reader.GetString(1);
                            lblWiN2.Text = reader.GetString(2);
                            lblWiH2.Text = reader.GetInt32(3).ToString();
                            lblWiM2.Text = reader.GetInt32(4).ToString();
                            lblWiA2.Text = reader.GetInt32(5).ToString();
                            lblWiS2.Text = reader.GetString(6);


                            reader.Read();
                            lblWiT3.Text = reader.GetString(1);
                            lblWiN3.Text = reader.GetString(2);
                            lblWiH3.Text = reader.GetInt32(3).ToString();
                            lblWiM3.Text = reader.GetInt32(4).ToString();
                            lblWiA3.Text = reader.GetInt32(5).ToString();
                            lblWiS3.Text = reader.GetString(6);
                        }
                    }

                    string sqlBeast = "SELECT * FROM Beast";

                    using (SqlCommand command = new SqlCommand(sqlBeast, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            reader.Read();
                            lblT1.Text = reader.GetString(1);
                            lblN1.Text = reader.GetString(2);
                            lblH1.Text = reader.GetInt32(3).ToString();
                            lblM1.Text = reader.GetInt32(4).ToString();
                            lblA1.Text = reader.GetInt32(5).ToString();
                            lblR1.Text = reader.GetInt32(6).ToString();

                            reader.Read();
                            lblT2.Text = reader.GetString(1);
                            lblN2.Text = reader.GetString(2);
                            lblH2.Text = reader.GetInt32(3).ToString();
                            lblM2.Text = reader.GetInt32(4).ToString();
                            lblA2.Text = reader.GetInt32(5).ToString();
                            lblR2.Text = reader.GetInt32(6).ToString();


                            reader.Read();
                            lblT3.Text = reader.GetString(1);
                            lblN3.Text = reader.GetString(2);
                            lblH3.Text = reader.GetInt32(3).ToString();
                            lblM3.Text = reader.GetInt32(4).ToString();
                            lblA3.Text = reader.GetInt32(5).ToString();
                            lblR3.Text = reader.GetInt32(6).ToString();


                        }
                    }

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sqlThief = "SELECT * FROM Thief";

                    using (SqlCommand command = new SqlCommand(sqlThief, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            Console.WriteLine($"{reader.GetName(0)} \t {reader.GetName(1)}");
                            Console.WriteLine("--------------------------------");



                            reader.Read();
                            lblThiT1.Text = reader.GetString(1);
                            lblThiN1.Text = reader.GetString(2);
                            lblThiH1.Text = reader.GetInt32(3).ToString();
                            lblThiM1.Text = reader.GetInt32(4).ToString();
                            lblThiA1.Text = reader.GetInt32(5).ToString();
                            lblThiS1.Text = reader.GetDouble(6).ToString();

                            reader.Read();
                            lblThiT2.Text = reader.GetString(1);
                            lblThiN2.Text = reader.GetString(2);
                            lblThiH2.Text = reader.GetInt32(3).ToString();
                            lblThiM2.Text = reader.GetInt32(4).ToString();
                            lblThiA2.Text = reader.GetInt32(5).ToString();
                            lblThiS2.Text = reader.GetDouble(6).ToString();


                            reader.Read();
                            lblThiT3.Text = reader.GetString(1);
                            lblThiN3.Text = reader.GetString(2);
                            lblThiH3.Text = reader.GetInt32(3).ToString();
                            lblThiM3.Text = reader.GetInt32(4).ToString();
                            lblThiA3.Text = reader.GetInt32(5).ToString();
                            lblThiS3.Text = reader.GetDouble(6).ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
