﻿using RPGCharacters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppCore
{
    public partial class WarriorForm : Form
    {
        public WarriorForm()
        {
            InitializeComponent();
        }

        // Redirect to the form where characters are listed
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListAllCharacters f2 = new ListAllCharacters();
            f2.ShowDialog();
        }


        // Method that deletes the selected character from the database
        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sql = "DELETE FROM Warrior WHERE ID = @warriorID";
                    string warriorID = listBox1.SelectedItem.ToString().Split(" ")[0]; 

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@warriorID", warriorID);

                        command.ExecuteNonQuery();
                    }
                    listBox1.Items.Clear();
                    WarriorForm_Load(sender, e);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }        
        }

        // Loading all warriors from database to the listbox
        private void WarriorForm_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;
            
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sqlWarrior = "SELECT * FROM Warrior";

                    using (SqlCommand command = new SqlCommand(sqlWarrior, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            listBox1.Items.Add($"{reader.GetName(0)} \t{reader.GetName(1)} \t {reader.GetName(2)} \t {reader.GetName(3)} \t {reader.GetName(4)} \t {reader.GetName(5)} \t {reader.GetName(6)}");

                            while (reader.Read())
                            {
                                listBox1.Items.Add($"{reader.GetInt32(0)} \t {reader.GetString(1)} \t \t {reader.GetString(2)} \t {reader.GetInt32(3)} \t {reader.GetInt32(4)} \t {reader.GetInt32(5)} \t {reader.GetString(6)}");

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Display the selected data in textfields so a user can edit them
        private void button2_Click(object sender, EventArgs e)
        {
            string name = listBox1.SelectedItem.ToString().Split(" ")[5];
            txtName.Text = name;

            string WarriorHP = listBox1.SelectedItem.ToString().Split(" ")[7];
            txtHP.Text = WarriorHP;

            string WarriorMana = listBox1.SelectedItem.ToString().Split(" ")[9];
            txtMana.Text = WarriorMana;

            string WarriorArmor = listBox1.SelectedItem.ToString().Split(" ")[11];
            txtArmor.Text = WarriorArmor;

            string WarriorSpecial = listBox1.SelectedItem.ToString().Split(" ")[13];
            txtSpecial.Text = WarriorSpecial;

        }

        //Method that updates the selected changes and stores them in the database
        private void button3_Click(object sender, EventArgs e)
        {
            string url = "UPDATE Warrior Set Name = @name, HP = @hp, Mana = @mana, ArmorRating = @armorRating, EquiptedShield = @equiptedShield Where ID = @warriorID";

            string warriorID = listBox1.SelectedItem.ToString().Split(" ")[0];
            string name = txtName.Text;
            int hp = int.Parse(txtHP.Text);
            int mana = int.Parse(txtMana.Text);
            int armorRating = int.Parse(txtArmor.Text);
            string equiptedShield = txtSpecial.Text;

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(url, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@hp", hp);
                        command.Parameters.AddWithValue("@mana", mana);
                        command.Parameters.AddWithValue("@armorRating", armorRating);
                        command.Parameters.AddWithValue("@equiptedShield", equiptedShield);
                        command.Parameters.AddWithValue("@warriorID", warriorID);

                        command.ExecuteNonQuery();

                    }
                    listBox1.Items.Clear();
                    WarriorForm_Load(sender, e);

                    txtName.Clear();
                    txtHP.Clear();
                    txtMana.Clear();
                    txtArmor.Clear();
                    txtSpecial.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
