﻿namespace WindowsFormsAppCore
{
    partial class WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tx = new System.Windows.Forms.Label();
            this.a = new System.Windows.Forms.Label();
            this.r = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtHp = new System.Windows.Forms.TextBox();
            this.txtMana = new System.Windows.Forms.TextBox();
            this.txtArmor = new System.Windows.Forms.TextBox();
            this.txtSpecial = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(31, 64);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(608, 344);
            this.listBox1.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(660, 315);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(135, 56);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "Go Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(660, 94);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(135, 71);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete Selected Wizard";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 428);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 496);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Health:";
            // 
            // tx
            // 
            this.tx.AutoSize = true;
            this.tx.Location = new System.Drawing.Point(254, 428);
            this.tx.Name = "tx";
            this.tx.Size = new System.Drawing.Size(100, 20);
            this.tx.TabIndex = 6;
            this.tx.Text = "Mana/Energy:";
            // 
            // a
            // 
            this.a.AutoSize = true;
            this.a.Location = new System.Drawing.Point(254, 496);
            this.a.Name = "a";
            this.a.Size = new System.Drawing.Size(101, 20);
            this.a.TabIndex = 7;
            this.a.Text = "Armor Rating:";
            // 
            // r
            // 
            this.r.AutoSize = true;
            this.r.Location = new System.Drawing.Point(520, 428);
            this.r.Name = "r";
            this.r.Size = new System.Drawing.Size(51, 20);
            this.r.TabIndex = 8;
            this.r.Text = "Spells:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(88, 428);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(125, 27);
            this.txtName.TabIndex = 9;
            // 
            // txtHp
            // 
            this.txtHp.Location = new System.Drawing.Point(88, 496);
            this.txtHp.Name = "txtHp";
            this.txtHp.Size = new System.Drawing.Size(125, 27);
            this.txtHp.TabIndex = 10;
            // 
            // txtMana
            // 
            this.txtMana.Location = new System.Drawing.Point(358, 428);
            this.txtMana.Name = "txtMana";
            this.txtMana.Size = new System.Drawing.Size(125, 27);
            this.txtMana.TabIndex = 11;
            // 
            // txtArmor
            // 
            this.txtArmor.Location = new System.Drawing.Point(358, 493);
            this.txtArmor.Name = "txtArmor";
            this.txtArmor.Size = new System.Drawing.Size(125, 27);
            this.txtArmor.TabIndex = 12;
            // 
            // txtSpecial
            // 
            this.txtSpecial.Location = new System.Drawing.Point(576, 428);
            this.txtSpecial.Name = "txtSpecial";
            this.txtSpecial.Size = new System.Drawing.Size(125, 27);
            this.txtSpecial.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(576, 470);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 46);
            this.button1.TabIndex = 14;
            this.button1.Text = "Apply Changes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(660, 207);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 61);
            this.button2.TabIndex = 15;
            this.button2.Text = "Update Selected Wizard";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(31, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 46);
            this.label3.TabIndex = 16;
            this.label3.Text = "Wizards:";
            // 
            // WizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 536);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtSpecial);
            this.Controls.Add(this.txtArmor);
            this.Controls.Add(this.txtMana);
            this.Controls.Add(this.txtHp);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.r);
            this.Controls.Add(this.a);
            this.Controls.Add(this.tx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.listBox1);
            this.Name = "WizardForm";
            this.Text = "a";
            this.Load += new System.EventHandler(this.WizardForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label tx;
        private System.Windows.Forms.Label a;
        private System.Windows.Forms.Label r;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtHp;
        private System.Windows.Forms.TextBox txtMana;
        private System.Windows.Forms.TextBox txtArmor;
        private System.Windows.Forms.TextBox txtSpecial;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
    }
}