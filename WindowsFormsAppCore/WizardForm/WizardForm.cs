﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppCore
{
    public partial class WizardForm : Form
    {
        public WizardForm()
        {
            InitializeComponent();
        }

        // Back method that redirects to the previous page with all characters listed
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListAllCharacters f2 = new ListAllCharacters();
            f2.ShowDialog();
        }

        // Method that deletes the selected wizard from the database
        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sql = "DELETE FROM Wizard WHERE ID = @wizardID";
                    string wizardID = listBox1.SelectedItem.ToString().Split(" ")[0];
                    MessageBox.Show(wizardID);

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@wizardID", wizardID);

                        command.ExecuteNonQuery();
                      
                    }

                    listBox1.Items.Clear();
                    WizardForm_Load(sender, e);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that loads all wizards into the listbox
        private void WizardForm_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sqlWizard = "SELECT * FROM Wizard";

                    using (SqlCommand command = new SqlCommand(sqlWizard, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            listBox1.Items.Add($"{reader.GetName(0)} \t{reader.GetName(1)} \t {reader.GetName(2)} \t {reader.GetName(3)} \t {reader.GetName(4)} \t {reader.GetName(5)} \t {reader.GetName(6)}");

                            while (reader.Read())
                            {
                                listBox1.Items.Add($"{reader.GetInt32(0)} \t {reader.GetString(1)} \t \t {reader.GetString(2)} \t {reader.GetInt32(3)} \t {reader.GetInt32(4)} \t \t {reader.GetInt32(5)} \t {reader.GetString(6)}");

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Method that updates the changes and stores them in the database
        private void button1_Click(object sender, EventArgs e)
        {
            string url = "UPDATE Wizard Set Name = @name, HP = @hp, Mana = @mana, ArmorRating = @armorRating, Spells = @spells Where ID = @wizardID";

            string wizardID = listBox1.SelectedItem.ToString().Split(" ")[0];
            string name = txtName.Text;
            int hp = int.Parse(txtHp.Text);
            int mana = int.Parse(txtMana.Text);
            int armorRating = int.Parse(txtArmor.Text);
            string spells = txtSpecial.Text;

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(url, connection))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@hp", hp);
                        command.Parameters.AddWithValue("@mana", mana);
                        command.Parameters.AddWithValue("@armorRating", armorRating);
                        command.Parameters.AddWithValue("@spells", spells);
                        command.Parameters.AddWithValue("@wizardID", wizardID);

                        command.ExecuteNonQuery();

                    }
                    listBox1.Items.Clear();
                    WizardForm_Load(sender, e);

                    txtName.Clear();
                    txtHp.Clear();
                    txtMana.Clear();
                    txtArmor.Clear();
                    txtSpecial.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method that brings the selected attributes and stores them in textboses for the user to change
        private void button2_Click(object sender, EventArgs e)
        {
            string name = listBox1.SelectedItem.ToString().Split(" ")[5];
            txtName.Text = name;

            string WizardHP = listBox1.SelectedItem.ToString().Split(" ")[7];
            txtHp.Text = WizardHP;

            string WizardMana = listBox1.SelectedItem.ToString().Split(" ")[9];
            txtMana.Text = WizardMana;

            string WizardArmor = listBox1.SelectedItem.ToString().Split(" ")[12];
            txtArmor.Text = WizardArmor;

            string WizardSpecial = listBox1.SelectedItem.ToString().Split(" ")[14];
            string WizardSpecial2 = listBox1.SelectedItem.ToString().Split(" ")[15];

            txtSpecial.Text = WizardSpecial + " " +WizardSpecial2;
        }
    }
}
