﻿namespace WindowsFormsAppCore
{
    partial class ListAllCharacters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblH3 = new System.Windows.Forms.Label();
            this.lblN3 = new System.Windows.Forms.Label();
            this.lblT3 = new System.Windows.Forms.Label();
            this.lblR3 = new System.Windows.Forms.Label();
            this.lblA3 = new System.Windows.Forms.Label();
            this.lblM3 = new System.Windows.Forms.Label();
            this.lblH2 = new System.Windows.Forms.Label();
            this.lblN2 = new System.Windows.Forms.Label();
            this.lblT2 = new System.Windows.Forms.Label();
            this.lblR2 = new System.Windows.Forms.Label();
            this.lblA2 = new System.Windows.Forms.Label();
            this.lblM2 = new System.Windows.Forms.Label();
            this.lblH1 = new System.Windows.Forms.Label();
            this.lblN1 = new System.Windows.Forms.Label();
            this.lblT1 = new System.Windows.Forms.Label();
            this.lblR1 = new System.Windows.Forms.Label();
            this.lblA1 = new System.Windows.Forms.Label();
            this.lblM1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.lblWiH3 = new System.Windows.Forms.Label();
            this.lblWiN3 = new System.Windows.Forms.Label();
            this.lblWiT3 = new System.Windows.Forms.Label();
            this.lblWiS3 = new System.Windows.Forms.Label();
            this.lblWiA3 = new System.Windows.Forms.Label();
            this.lblWiM3 = new System.Windows.Forms.Label();
            this.lblWiH2 = new System.Windows.Forms.Label();
            this.lblWiN2 = new System.Windows.Forms.Label();
            this.lblWiT2 = new System.Windows.Forms.Label();
            this.lblWiS2 = new System.Windows.Forms.Label();
            this.lblWiA2 = new System.Windows.Forms.Label();
            this.lblWiM2 = new System.Windows.Forms.Label();
            this.lblWiH1 = new System.Windows.Forms.Label();
            this.lblWiN1 = new System.Windows.Forms.Label();
            this.lblWiT1 = new System.Windows.Forms.Label();
            this.lblWiS1 = new System.Windows.Forms.Label();
            this.lblWiA1 = new System.Windows.Forms.Label();
            this.lblWiM1 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblThiH3 = new System.Windows.Forms.Label();
            this.lblThiN3 = new System.Windows.Forms.Label();
            this.lblThiT3 = new System.Windows.Forms.Label();
            this.lblThiS3 = new System.Windows.Forms.Label();
            this.lblThiA3 = new System.Windows.Forms.Label();
            this.lblThiM3 = new System.Windows.Forms.Label();
            this.lblThiH2 = new System.Windows.Forms.Label();
            this.lblThiN2 = new System.Windows.Forms.Label();
            this.lblThiT2 = new System.Windows.Forms.Label();
            this.lblThiS2 = new System.Windows.Forms.Label();
            this.lblThiA2 = new System.Windows.Forms.Label();
            this.lblThiM2 = new System.Windows.Forms.Label();
            this.lblThiH1 = new System.Windows.Forms.Label();
            this.lblThiN1 = new System.Windows.Forms.Label();
            this.lblThiT1 = new System.Windows.Forms.Label();
            this.lblThiS1 = new System.Windows.Forms.Label();
            this.lblThiA1 = new System.Windows.Forms.Label();
            this.lblThiM1 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblWaH3 = new System.Windows.Forms.Label();
            this.lblWaN3 = new System.Windows.Forms.Label();
            this.lblWaT3 = new System.Windows.Forms.Label();
            this.lblWaS3 = new System.Windows.Forms.Label();
            this.lblWaA3 = new System.Windows.Forms.Label();
            this.lblWaM3 = new System.Windows.Forms.Label();
            this.lblWaH2 = new System.Windows.Forms.Label();
            this.lblWaN2 = new System.Windows.Forms.Label();
            this.lblWaT2 = new System.Windows.Forms.Label();
            this.lblWaS2 = new System.Windows.Forms.Label();
            this.lblWaA2 = new System.Windows.Forms.Label();
            this.lblWaM2 = new System.Windows.Forms.Label();
            this.lblWaH1 = new System.Windows.Forms.Label();
            this.lblWaN1 = new System.Windows.Forms.Label();
            this.lblWaT1 = new System.Windows.Forms.Label();
            this.lblWaS1 = new System.Windows.Forms.Label();
            this.lblWaA1 = new System.Windows.Forms.Label();
            this.lblWaM1 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUpDelThief = new System.Windows.Forms.Button();
            this.btnDelUpWizard = new System.Windows.Forms.Button();
            this.btnUpDelWarrior = new System.Windows.Forms.Button();
            this.btnUpDelBeast = new System.Windows.Forms.Button();
            this.btnShowWarrior = new System.Windows.Forms.Button();
            this.btnShowWizard = new System.Windows.Forms.Button();
            this.btnShowBeasts = new System.Windows.Forms.Button();
            this.btnThiefShow = new System.Windows.Forms.Button();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-185, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(245, 1);
            this.label7.TabIndex = 0;
            this.label7.Text = "Special Ability";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(-297, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 1);
            this.label8.TabIndex = 0;
            this.label8.Text = "ArmorRating";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-391, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 1);
            this.label9.TabIndex = 0;
            this.label9.Text = "Mana";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(-495, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 1);
            this.label10.TabIndex = 0;
            this.label10.Text = "HP/Hitpoints";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(-246, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(1, 1);
            this.label11.TabIndex = 0;
            this.label11.Text = "Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(1, 1);
            this.label12.TabIndex = 0;
            this.label12.Text = "Type";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 251F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 25);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.27586F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.72414F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(62, 18);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(449, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rage Mode Level";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(337, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "ArmorRating";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(243, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mana";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "HP/Hitpoints";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(71, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Type";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 252F));
            this.tableLayoutPanel2.Controls.Add(this.lblH3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblN3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblT3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblR3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblA3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblM3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblH2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblN2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblT2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblR2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblA2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblM2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblH1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblN1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblT1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblR1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblA1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblM1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(35, 177);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.27586F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(698, 137);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lblH3
            // 
            this.lblH3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblH3.AutoSize = true;
            this.lblH3.Location = new System.Drawing.Point(139, 113);
            this.lblH3.Name = "lblH3";
            this.lblH3.Size = new System.Drawing.Size(98, 20);
            this.lblH3.TabIndex = 0;
            this.lblH3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblN3
            // 
            this.lblN3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblN3.AutoSize = true;
            this.lblN3.Location = new System.Drawing.Point(71, 113);
            this.lblN3.Name = "lblN3";
            this.lblN3.Size = new System.Drawing.Size(62, 20);
            this.lblN3.TabIndex = 0;
            this.lblN3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblT3
            // 
            this.lblT3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblT3.AutoSize = true;
            this.lblT3.Location = new System.Drawing.Point(3, 113);
            this.lblT3.Name = "lblT3";
            this.lblT3.Size = new System.Drawing.Size(62, 20);
            this.lblT3.TabIndex = 0;
            this.lblT3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblR3
            // 
            this.lblR3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblR3.AutoSize = true;
            this.lblR3.Location = new System.Drawing.Point(449, 113);
            this.lblR3.Name = "lblR3";
            this.lblR3.Size = new System.Drawing.Size(246, 20);
            this.lblR3.TabIndex = 0;
            this.lblR3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblA3
            // 
            this.lblA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblA3.AutoSize = true;
            this.lblA3.Location = new System.Drawing.Point(337, 113);
            this.lblA3.Name = "lblA3";
            this.lblA3.Size = new System.Drawing.Size(106, 20);
            this.lblA3.TabIndex = 0;
            this.lblA3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblM3
            // 
            this.lblM3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblM3.AutoSize = true;
            this.lblM3.Location = new System.Drawing.Point(243, 113);
            this.lblM3.Name = "lblM3";
            this.lblM3.Size = new System.Drawing.Size(88, 20);
            this.lblM3.TabIndex = 0;
            this.lblM3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblH2
            // 
            this.lblH2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblH2.AutoSize = true;
            this.lblH2.Location = new System.Drawing.Point(139, 84);
            this.lblH2.Name = "lblH2";
            this.lblH2.Size = new System.Drawing.Size(98, 20);
            this.lblH2.TabIndex = 0;
            this.lblH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblN2
            // 
            this.lblN2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblN2.AutoSize = true;
            this.lblN2.Location = new System.Drawing.Point(71, 84);
            this.lblN2.Name = "lblN2";
            this.lblN2.Size = new System.Drawing.Size(62, 20);
            this.lblN2.TabIndex = 0;
            this.lblN2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblT2
            // 
            this.lblT2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblT2.AutoSize = true;
            this.lblT2.Location = new System.Drawing.Point(3, 84);
            this.lblT2.Name = "lblT2";
            this.lblT2.Size = new System.Drawing.Size(62, 20);
            this.lblT2.TabIndex = 0;
            this.lblT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblR2
            // 
            this.lblR2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblR2.AutoSize = true;
            this.lblR2.Location = new System.Drawing.Point(449, 84);
            this.lblR2.Name = "lblR2";
            this.lblR2.Size = new System.Drawing.Size(246, 20);
            this.lblR2.TabIndex = 0;
            this.lblR2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblA2
            // 
            this.lblA2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblA2.AutoSize = true;
            this.lblA2.Location = new System.Drawing.Point(337, 84);
            this.lblA2.Name = "lblA2";
            this.lblA2.Size = new System.Drawing.Size(106, 20);
            this.lblA2.TabIndex = 0;
            this.lblA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblM2
            // 
            this.lblM2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblM2.AutoSize = true;
            this.lblM2.Location = new System.Drawing.Point(243, 84);
            this.lblM2.Name = "lblM2";
            this.lblM2.Size = new System.Drawing.Size(88, 20);
            this.lblM2.TabIndex = 0;
            this.lblM2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblH1
            // 
            this.lblH1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblH1.AutoSize = true;
            this.lblH1.Location = new System.Drawing.Point(139, 53);
            this.lblH1.Name = "lblH1";
            this.lblH1.Size = new System.Drawing.Size(98, 20);
            this.lblH1.TabIndex = 0;
            this.lblH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblN1
            // 
            this.lblN1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblN1.AutoSize = true;
            this.lblN1.Location = new System.Drawing.Point(71, 53);
            this.lblN1.Name = "lblN1";
            this.lblN1.Size = new System.Drawing.Size(62, 20);
            this.lblN1.TabIndex = 0;
            this.lblN1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblT1
            // 
            this.lblT1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblT1.AutoSize = true;
            this.lblT1.Location = new System.Drawing.Point(3, 53);
            this.lblT1.Name = "lblT1";
            this.lblT1.Size = new System.Drawing.Size(62, 20);
            this.lblT1.TabIndex = 0;
            this.lblT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblR1
            // 
            this.lblR1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblR1.AutoSize = true;
            this.lblR1.Location = new System.Drawing.Point(449, 53);
            this.lblR1.Name = "lblR1";
            this.lblR1.Size = new System.Drawing.Size(246, 20);
            this.lblR1.TabIndex = 0;
            this.lblR1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblA1
            // 
            this.lblA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblA1.AutoSize = true;
            this.lblA1.Location = new System.Drawing.Point(337, 53);
            this.lblA1.Name = "lblA1";
            this.lblA1.Size = new System.Drawing.Size(106, 20);
            this.lblA1.TabIndex = 0;
            this.lblA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblM1
            // 
            this.lblM1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblM1.AutoSize = true;
            this.lblM1.Location = new System.Drawing.Point(243, 53);
            this.lblM1.Name = "lblM1";
            this.lblM1.Size = new System.Drawing.Size(88, 20);
            this.lblM1.TabIndex = 0;
            this.lblM1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(449, 56);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(246, 20);
            this.label33.TabIndex = 0;
            this.label33.Text = "Type";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(3, 82);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(62, 20);
            this.label34.TabIndex = 0;
            this.label34.Text = "Type";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(71, 56);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(62, 20);
            this.label35.TabIndex = 0;
            this.label35.Text = "Type";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(139, 56);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(98, 20);
            this.label36.TabIndex = 0;
            this.label36.Text = "Type";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiH3
            // 
            this.lblWiH3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiH3.AutoSize = true;
            this.lblWiH3.Location = new System.Drawing.Point(139, 113);
            this.lblWiH3.Name = "lblWiH3";
            this.lblWiH3.Size = new System.Drawing.Size(98, 20);
            this.lblWiH3.TabIndex = 0;
            this.lblWiH3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiN3
            // 
            this.lblWiN3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiN3.AutoSize = true;
            this.lblWiN3.Location = new System.Drawing.Point(71, 113);
            this.lblWiN3.Name = "lblWiN3";
            this.lblWiN3.Size = new System.Drawing.Size(62, 20);
            this.lblWiN3.TabIndex = 0;
            this.lblWiN3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiT3
            // 
            this.lblWiT3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiT3.AutoSize = true;
            this.lblWiT3.Location = new System.Drawing.Point(3, 113);
            this.lblWiT3.Name = "lblWiT3";
            this.lblWiT3.Size = new System.Drawing.Size(62, 20);
            this.lblWiT3.TabIndex = 0;
            this.lblWiT3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiS3
            // 
            this.lblWiS3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiS3.AutoSize = true;
            this.lblWiS3.Location = new System.Drawing.Point(449, 113);
            this.lblWiS3.Name = "lblWiS3";
            this.lblWiS3.Size = new System.Drawing.Size(246, 20);
            this.lblWiS3.TabIndex = 0;
            this.lblWiS3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiA3
            // 
            this.lblWiA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiA3.AutoSize = true;
            this.lblWiA3.Location = new System.Drawing.Point(337, 113);
            this.lblWiA3.Name = "lblWiA3";
            this.lblWiA3.Size = new System.Drawing.Size(106, 20);
            this.lblWiA3.TabIndex = 0;
            this.lblWiA3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiM3
            // 
            this.lblWiM3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiM3.AutoSize = true;
            this.lblWiM3.Location = new System.Drawing.Point(243, 113);
            this.lblWiM3.Name = "lblWiM3";
            this.lblWiM3.Size = new System.Drawing.Size(88, 20);
            this.lblWiM3.TabIndex = 0;
            this.lblWiM3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiH2
            // 
            this.lblWiH2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiH2.AutoSize = true;
            this.lblWiH2.Location = new System.Drawing.Point(139, 84);
            this.lblWiH2.Name = "lblWiH2";
            this.lblWiH2.Size = new System.Drawing.Size(98, 20);
            this.lblWiH2.TabIndex = 0;
            this.lblWiH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiN2
            // 
            this.lblWiN2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiN2.AutoSize = true;
            this.lblWiN2.Location = new System.Drawing.Point(71, 84);
            this.lblWiN2.Name = "lblWiN2";
            this.lblWiN2.Size = new System.Drawing.Size(62, 20);
            this.lblWiN2.TabIndex = 0;
            this.lblWiN2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiT2
            // 
            this.lblWiT2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiT2.AutoSize = true;
            this.lblWiT2.Location = new System.Drawing.Point(3, 84);
            this.lblWiT2.Name = "lblWiT2";
            this.lblWiT2.Size = new System.Drawing.Size(62, 20);
            this.lblWiT2.TabIndex = 0;
            this.lblWiT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiS2
            // 
            this.lblWiS2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiS2.AutoSize = true;
            this.lblWiS2.Location = new System.Drawing.Point(449, 84);
            this.lblWiS2.Name = "lblWiS2";
            this.lblWiS2.Size = new System.Drawing.Size(246, 20);
            this.lblWiS2.TabIndex = 0;
            this.lblWiS2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiA2
            // 
            this.lblWiA2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiA2.AutoSize = true;
            this.lblWiA2.Location = new System.Drawing.Point(337, 84);
            this.lblWiA2.Name = "lblWiA2";
            this.lblWiA2.Size = new System.Drawing.Size(106, 20);
            this.lblWiA2.TabIndex = 0;
            this.lblWiA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiM2
            // 
            this.lblWiM2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiM2.AutoSize = true;
            this.lblWiM2.Location = new System.Drawing.Point(243, 84);
            this.lblWiM2.Name = "lblWiM2";
            this.lblWiM2.Size = new System.Drawing.Size(88, 20);
            this.lblWiM2.TabIndex = 0;
            this.lblWiM2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiH1
            // 
            this.lblWiH1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiH1.AutoSize = true;
            this.lblWiH1.Location = new System.Drawing.Point(139, 53);
            this.lblWiH1.Name = "lblWiH1";
            this.lblWiH1.Size = new System.Drawing.Size(98, 20);
            this.lblWiH1.TabIndex = 0;
            this.lblWiH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiN1
            // 
            this.lblWiN1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiN1.AutoSize = true;
            this.lblWiN1.Location = new System.Drawing.Point(71, 53);
            this.lblWiN1.Name = "lblWiN1";
            this.lblWiN1.Size = new System.Drawing.Size(62, 20);
            this.lblWiN1.TabIndex = 0;
            this.lblWiN1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiT1
            // 
            this.lblWiT1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiT1.AutoSize = true;
            this.lblWiT1.Location = new System.Drawing.Point(3, 53);
            this.lblWiT1.Name = "lblWiT1";
            this.lblWiT1.Size = new System.Drawing.Size(62, 20);
            this.lblWiT1.TabIndex = 0;
            this.lblWiT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiS1
            // 
            this.lblWiS1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiS1.AutoSize = true;
            this.lblWiS1.Location = new System.Drawing.Point(449, 53);
            this.lblWiS1.Name = "lblWiS1";
            this.lblWiS1.Size = new System.Drawing.Size(246, 20);
            this.lblWiS1.TabIndex = 0;
            this.lblWiS1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiA1
            // 
            this.lblWiA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiA1.AutoSize = true;
            this.lblWiA1.Location = new System.Drawing.Point(337, 53);
            this.lblWiA1.Name = "lblWiA1";
            this.lblWiA1.Size = new System.Drawing.Size(106, 20);
            this.lblWiA1.TabIndex = 0;
            this.lblWiA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiM1
            // 
            this.lblWiM1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWiM1.AutoSize = true;
            this.lblWiM1.Location = new System.Drawing.Point(243, 53);
            this.lblWiM1.Name = "lblWiM1";
            this.lblWiM1.Size = new System.Drawing.Size(88, 20);
            this.lblWiM1.TabIndex = 0;
            this.lblWiM1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(449, 14);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(246, 20);
            this.label58.TabIndex = 0;
            this.label58.Text = "Spells Ability";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(337, 14);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(106, 20);
            this.label59.TabIndex = 0;
            this.label59.Text = "ArmorRating";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label60
            // 
            this.label60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(243, 14);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(88, 20);
            this.label60.TabIndex = 0;
            this.label60.Text = "Mana";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label61
            // 
            this.label61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(139, 14);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(98, 20);
            this.label61.TabIndex = 0;
            this.label61.Text = "HP/Hitpoints";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(71, 14);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(62, 20);
            this.label62.TabIndex = 0;
            this.label62.Text = "Name";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label63
            // 
            this.label63.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(3, 14);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(62, 20);
            this.label63.TabIndex = 0;
            this.label63.Text = "Type";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 6;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 252F));
            this.tableLayoutPanel4.Controls.Add(this.lblWiH3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblWiN3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblWiT3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblWiS3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblWiA3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblWiM3, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.lblWiH2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblWiN2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblWiT2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblWiS2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblWiA2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblWiM2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblWiH1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblWiN1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblWiT1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblWiS1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblWiA1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblWiM1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label58, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.label59, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.label60, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.label61, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label62, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label63, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(35, 333);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.27586F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(698, 137);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // lblThiH3
            // 
            this.lblThiH3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiH3.AutoSize = true;
            this.lblThiH3.Location = new System.Drawing.Point(139, 113);
            this.lblThiH3.Name = "lblThiH3";
            this.lblThiH3.Size = new System.Drawing.Size(98, 20);
            this.lblThiH3.TabIndex = 0;
            this.lblThiH3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiN3
            // 
            this.lblThiN3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiN3.AutoSize = true;
            this.lblThiN3.Location = new System.Drawing.Point(71, 113);
            this.lblThiN3.Name = "lblThiN3";
            this.lblThiN3.Size = new System.Drawing.Size(62, 20);
            this.lblThiN3.TabIndex = 0;
            this.lblThiN3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiT3
            // 
            this.lblThiT3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiT3.AutoSize = true;
            this.lblThiT3.Location = new System.Drawing.Point(3, 113);
            this.lblThiT3.Name = "lblThiT3";
            this.lblThiT3.Size = new System.Drawing.Size(62, 20);
            this.lblThiT3.TabIndex = 0;
            this.lblThiT3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiS3
            // 
            this.lblThiS3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiS3.AutoSize = true;
            this.lblThiS3.Location = new System.Drawing.Point(449, 113);
            this.lblThiS3.Name = "lblThiS3";
            this.lblThiS3.Size = new System.Drawing.Size(246, 20);
            this.lblThiS3.TabIndex = 0;
            this.lblThiS3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiA3
            // 
            this.lblThiA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiA3.AutoSize = true;
            this.lblThiA3.Location = new System.Drawing.Point(337, 113);
            this.lblThiA3.Name = "lblThiA3";
            this.lblThiA3.Size = new System.Drawing.Size(106, 20);
            this.lblThiA3.TabIndex = 0;
            this.lblThiA3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiM3
            // 
            this.lblThiM3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiM3.AutoSize = true;
            this.lblThiM3.Location = new System.Drawing.Point(243, 113);
            this.lblThiM3.Name = "lblThiM3";
            this.lblThiM3.Size = new System.Drawing.Size(88, 20);
            this.lblThiM3.TabIndex = 0;
            this.lblThiM3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiH2
            // 
            this.lblThiH2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiH2.AutoSize = true;
            this.lblThiH2.Location = new System.Drawing.Point(139, 84);
            this.lblThiH2.Name = "lblThiH2";
            this.lblThiH2.Size = new System.Drawing.Size(98, 20);
            this.lblThiH2.TabIndex = 0;
            this.lblThiH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiN2
            // 
            this.lblThiN2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiN2.AutoSize = true;
            this.lblThiN2.Location = new System.Drawing.Point(71, 84);
            this.lblThiN2.Name = "lblThiN2";
            this.lblThiN2.Size = new System.Drawing.Size(62, 20);
            this.lblThiN2.TabIndex = 0;
            this.lblThiN2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiT2
            // 
            this.lblThiT2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiT2.AutoSize = true;
            this.lblThiT2.Location = new System.Drawing.Point(3, 84);
            this.lblThiT2.Name = "lblThiT2";
            this.lblThiT2.Size = new System.Drawing.Size(62, 20);
            this.lblThiT2.TabIndex = 0;
            this.lblThiT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiS2
            // 
            this.lblThiS2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiS2.AutoSize = true;
            this.lblThiS2.Location = new System.Drawing.Point(449, 84);
            this.lblThiS2.Name = "lblThiS2";
            this.lblThiS2.Size = new System.Drawing.Size(246, 20);
            this.lblThiS2.TabIndex = 0;
            this.lblThiS2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiA2
            // 
            this.lblThiA2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiA2.AutoSize = true;
            this.lblThiA2.Location = new System.Drawing.Point(337, 84);
            this.lblThiA2.Name = "lblThiA2";
            this.lblThiA2.Size = new System.Drawing.Size(106, 20);
            this.lblThiA2.TabIndex = 0;
            this.lblThiA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiM2
            // 
            this.lblThiM2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiM2.AutoSize = true;
            this.lblThiM2.Location = new System.Drawing.Point(243, 84);
            this.lblThiM2.Name = "lblThiM2";
            this.lblThiM2.Size = new System.Drawing.Size(88, 20);
            this.lblThiM2.TabIndex = 0;
            this.lblThiM2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiH1
            // 
            this.lblThiH1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiH1.AutoSize = true;
            this.lblThiH1.Location = new System.Drawing.Point(139, 53);
            this.lblThiH1.Name = "lblThiH1";
            this.lblThiH1.Size = new System.Drawing.Size(98, 20);
            this.lblThiH1.TabIndex = 0;
            this.lblThiH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiN1
            // 
            this.lblThiN1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiN1.AutoSize = true;
            this.lblThiN1.Location = new System.Drawing.Point(71, 53);
            this.lblThiN1.Name = "lblThiN1";
            this.lblThiN1.Size = new System.Drawing.Size(62, 20);
            this.lblThiN1.TabIndex = 0;
            this.lblThiN1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiT1
            // 
            this.lblThiT1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiT1.AutoSize = true;
            this.lblThiT1.Location = new System.Drawing.Point(3, 53);
            this.lblThiT1.Name = "lblThiT1";
            this.lblThiT1.Size = new System.Drawing.Size(62, 20);
            this.lblThiT1.TabIndex = 0;
            this.lblThiT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiS1
            // 
            this.lblThiS1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiS1.AutoSize = true;
            this.lblThiS1.Location = new System.Drawing.Point(449, 53);
            this.lblThiS1.Name = "lblThiS1";
            this.lblThiS1.Size = new System.Drawing.Size(246, 20);
            this.lblThiS1.TabIndex = 0;
            this.lblThiS1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiA1
            // 
            this.lblThiA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiA1.AutoSize = true;
            this.lblThiA1.Location = new System.Drawing.Point(337, 53);
            this.lblThiA1.Name = "lblThiA1";
            this.lblThiA1.Size = new System.Drawing.Size(106, 20);
            this.lblThiA1.TabIndex = 0;
            this.lblThiA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThiM1
            // 
            this.lblThiM1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblThiM1.AutoSize = true;
            this.lblThiM1.Location = new System.Drawing.Point(243, 53);
            this.lblThiM1.Name = "lblThiM1";
            this.lblThiM1.Size = new System.Drawing.Size(88, 20);
            this.lblThiM1.TabIndex = 0;
            this.lblThiM1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(449, 14);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(246, 20);
            this.label41.TabIndex = 0;
            this.label41.Text = "Stealth Mode Duration Seconds";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(337, 14);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(106, 20);
            this.label42.TabIndex = 0;
            this.label42.Text = "ArmorRating";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(243, 14);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(88, 20);
            this.label43.TabIndex = 0;
            this.label43.Text = "Mana";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(139, 14);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(98, 20);
            this.label44.TabIndex = 0;
            this.label44.Text = "HP/Hitpoints";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(71, 14);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(62, 20);
            this.label45.TabIndex = 0;
            this.label45.Text = "Name";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 14);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(62, 20);
            this.label46.TabIndex = 0;
            this.label46.Text = "Type";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 252F));
            this.tableLayoutPanel1.Controls.Add(this.lblThiH3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThiN3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThiT3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThiS3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThiA3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThiM3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThiH2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblThiN2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblThiT2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblThiS2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblThiA2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblThiM2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblThiH1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblThiN1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblThiT1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblThiS1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblThiA1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblThiM1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label41, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label42, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label43, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label44, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label45, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label46, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(35, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.27586F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(698, 137);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblWaH3
            // 
            this.lblWaH3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaH3.AutoSize = true;
            this.lblWaH3.Location = new System.Drawing.Point(139, 113);
            this.lblWaH3.Name = "lblWaH3";
            this.lblWaH3.Size = new System.Drawing.Size(98, 20);
            this.lblWaH3.TabIndex = 0;
            this.lblWaH3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaN3
            // 
            this.lblWaN3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaN3.AutoSize = true;
            this.lblWaN3.Location = new System.Drawing.Point(71, 113);
            this.lblWaN3.Name = "lblWaN3";
            this.lblWaN3.Size = new System.Drawing.Size(62, 20);
            this.lblWaN3.TabIndex = 0;
            this.lblWaN3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaT3
            // 
            this.lblWaT3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaT3.AutoSize = true;
            this.lblWaT3.Location = new System.Drawing.Point(3, 113);
            this.lblWaT3.Name = "lblWaT3";
            this.lblWaT3.Size = new System.Drawing.Size(62, 20);
            this.lblWaT3.TabIndex = 0;
            this.lblWaT3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaS3
            // 
            this.lblWaS3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaS3.AutoSize = true;
            this.lblWaS3.Location = new System.Drawing.Point(449, 113);
            this.lblWaS3.Name = "lblWaS3";
            this.lblWaS3.Size = new System.Drawing.Size(246, 20);
            this.lblWaS3.TabIndex = 0;
            this.lblWaS3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaA3
            // 
            this.lblWaA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaA3.AutoSize = true;
            this.lblWaA3.Location = new System.Drawing.Point(337, 113);
            this.lblWaA3.Name = "lblWaA3";
            this.lblWaA3.Size = new System.Drawing.Size(106, 20);
            this.lblWaA3.TabIndex = 0;
            this.lblWaA3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaM3
            // 
            this.lblWaM3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaM3.AutoSize = true;
            this.lblWaM3.Location = new System.Drawing.Point(243, 113);
            this.lblWaM3.Name = "lblWaM3";
            this.lblWaM3.Size = new System.Drawing.Size(88, 20);
            this.lblWaM3.TabIndex = 0;
            this.lblWaM3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaH2
            // 
            this.lblWaH2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaH2.AutoSize = true;
            this.lblWaH2.Location = new System.Drawing.Point(139, 84);
            this.lblWaH2.Name = "lblWaH2";
            this.lblWaH2.Size = new System.Drawing.Size(98, 20);
            this.lblWaH2.TabIndex = 0;
            this.lblWaH2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaN2
            // 
            this.lblWaN2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaN2.AutoSize = true;
            this.lblWaN2.Location = new System.Drawing.Point(71, 84);
            this.lblWaN2.Name = "lblWaN2";
            this.lblWaN2.Size = new System.Drawing.Size(62, 20);
            this.lblWaN2.TabIndex = 0;
            this.lblWaN2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaT2
            // 
            this.lblWaT2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaT2.AutoSize = true;
            this.lblWaT2.Location = new System.Drawing.Point(3, 84);
            this.lblWaT2.Name = "lblWaT2";
            this.lblWaT2.Size = new System.Drawing.Size(62, 20);
            this.lblWaT2.TabIndex = 0;
            this.lblWaT2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaS2
            // 
            this.lblWaS2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaS2.AutoSize = true;
            this.lblWaS2.Location = new System.Drawing.Point(449, 84);
            this.lblWaS2.Name = "lblWaS2";
            this.lblWaS2.Size = new System.Drawing.Size(246, 20);
            this.lblWaS2.TabIndex = 0;
            this.lblWaS2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaA2
            // 
            this.lblWaA2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaA2.AutoSize = true;
            this.lblWaA2.Location = new System.Drawing.Point(337, 84);
            this.lblWaA2.Name = "lblWaA2";
            this.lblWaA2.Size = new System.Drawing.Size(106, 20);
            this.lblWaA2.TabIndex = 0;
            this.lblWaA2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaM2
            // 
            this.lblWaM2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaM2.AutoSize = true;
            this.lblWaM2.Location = new System.Drawing.Point(243, 84);
            this.lblWaM2.Name = "lblWaM2";
            this.lblWaM2.Size = new System.Drawing.Size(88, 20);
            this.lblWaM2.TabIndex = 0;
            this.lblWaM2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaH1
            // 
            this.lblWaH1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaH1.AutoSize = true;
            this.lblWaH1.Location = new System.Drawing.Point(139, 53);
            this.lblWaH1.Name = "lblWaH1";
            this.lblWaH1.Size = new System.Drawing.Size(98, 20);
            this.lblWaH1.TabIndex = 0;
            this.lblWaH1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaN1
            // 
            this.lblWaN1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaN1.AutoSize = true;
            this.lblWaN1.Location = new System.Drawing.Point(71, 53);
            this.lblWaN1.Name = "lblWaN1";
            this.lblWaN1.Size = new System.Drawing.Size(62, 20);
            this.lblWaN1.TabIndex = 0;
            this.lblWaN1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaT1
            // 
            this.lblWaT1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaT1.AutoSize = true;
            this.lblWaT1.Location = new System.Drawing.Point(3, 53);
            this.lblWaT1.Name = "lblWaT1";
            this.lblWaT1.Size = new System.Drawing.Size(62, 20);
            this.lblWaT1.TabIndex = 0;
            this.lblWaT1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaS1
            // 
            this.lblWaS1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaS1.AutoSize = true;
            this.lblWaS1.Location = new System.Drawing.Point(449, 53);
            this.lblWaS1.Name = "lblWaS1";
            this.lblWaS1.Size = new System.Drawing.Size(246, 20);
            this.lblWaS1.TabIndex = 0;
            this.lblWaS1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaA1
            // 
            this.lblWaA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaA1.AutoSize = true;
            this.lblWaA1.Location = new System.Drawing.Point(337, 53);
            this.lblWaA1.Name = "lblWaA1";
            this.lblWaA1.Size = new System.Drawing.Size(106, 20);
            this.lblWaA1.TabIndex = 0;
            this.lblWaA1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWaM1
            // 
            this.lblWaM1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWaM1.AutoSize = true;
            this.lblWaM1.Location = new System.Drawing.Point(243, 53);
            this.lblWaM1.Name = "lblWaM1";
            this.lblWaM1.Size = new System.Drawing.Size(88, 20);
            this.lblWaM1.TabIndex = 0;
            this.lblWaM1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(449, 14);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(246, 20);
            this.label65.TabIndex = 0;
            this.label65.Text = "Shield Equipted";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label66
            // 
            this.label66.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(337, 14);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(106, 20);
            this.label66.TabIndex = 0;
            this.label66.Text = "ArmorRating";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(243, 14);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(88, 20);
            this.label67.TabIndex = 0;
            this.label67.Text = "Mana";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(139, 14);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(98, 20);
            this.label68.TabIndex = 0;
            this.label68.Text = "HP/Hitpoints";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label69
            // 
            this.label69.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(71, 14);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(62, 20);
            this.label69.TabIndex = 0;
            this.label69.Text = "Name";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(3, 14);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(62, 20);
            this.label70.TabIndex = 0;
            this.label70.Text = "Type";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 252F));
            this.tableLayoutPanel5.Controls.Add(this.lblWaH3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblWaN3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblWaT3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblWaS3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblWaA3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblWaM3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblWaH2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblWaN2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblWaT2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblWaS2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblWaA2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblWaM2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblWaH1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblWaN1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblWaT1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblWaS1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblWaA1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblWaM1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label65, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.label66, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.label67, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.label68, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label69, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label70, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(35, 497);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.27586F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(698, 137);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // btnUpDelThief
            // 
            this.btnUpDelThief.Location = new System.Drawing.Point(748, 87);
            this.btnUpDelThief.Name = "btnUpDelThief";
            this.btnUpDelThief.Size = new System.Drawing.Size(136, 62);
            this.btnUpDelThief.TabIndex = 5;
            this.btnUpDelThief.Text = "Update or Delete Thiefs";
            this.btnUpDelThief.UseVisualStyleBackColor = true;
            this.btnUpDelThief.Click += new System.EventHandler(this.btnUpDelThief_Click);
            // 
            // btnDelUpWizard
            // 
            this.btnDelUpWizard.Location = new System.Drawing.Point(748, 402);
            this.btnDelUpWizard.Name = "btnDelUpWizard";
            this.btnDelUpWizard.Size = new System.Drawing.Size(136, 68);
            this.btnDelUpWizard.TabIndex = 6;
            this.btnDelUpWizard.Text = "Update or Delete Wizards";
            this.btnDelUpWizard.UseVisualStyleBackColor = true;
            this.btnDelUpWizard.Click += new System.EventHandler(this.btnDelUpWizard_Click);
            // 
            // btnUpDelWarrior
            // 
            this.btnUpDelWarrior.Location = new System.Drawing.Point(748, 570);
            this.btnUpDelWarrior.Name = "btnUpDelWarrior";
            this.btnUpDelWarrior.Size = new System.Drawing.Size(136, 64);
            this.btnUpDelWarrior.TabIndex = 7;
            this.btnUpDelWarrior.Text = "Update or Delete Warrior";
            this.btnUpDelWarrior.UseVisualStyleBackColor = true;
            this.btnUpDelWarrior.Click += new System.EventHandler(this.btnUpDelWarrior_Click);
            // 
            // btnUpDelBeast
            // 
            this.btnUpDelBeast.Location = new System.Drawing.Point(748, 247);
            this.btnUpDelBeast.Name = "btnUpDelBeast";
            this.btnUpDelBeast.Size = new System.Drawing.Size(136, 67);
            this.btnUpDelBeast.TabIndex = 8;
            this.btnUpDelBeast.Text = "Update or Delete Beasts";
            this.btnUpDelBeast.UseVisualStyleBackColor = true;
            this.btnUpDelBeast.Click += new System.EventHandler(this.btnUpDelBeast_Click);
            // 
            // btnShowWarrior
            // 
            this.btnShowWarrior.Location = new System.Drawing.Point(748, 497);
            this.btnShowWarrior.Name = "btnShowWarrior";
            this.btnShowWarrior.Size = new System.Drawing.Size(136, 63);
            this.btnShowWarrior.TabIndex = 3;
            this.btnShowWarrior.Text = "Create a Warrior";
            this.btnShowWarrior.UseVisualStyleBackColor = true;
            this.btnShowWarrior.Click += new System.EventHandler(this.btnShowWarrior_Click);
            // 
            // btnShowWizard
            // 
            this.btnShowWizard.Location = new System.Drawing.Point(748, 333);
            this.btnShowWizard.Name = "btnShowWizard";
            this.btnShowWizard.Size = new System.Drawing.Size(136, 63);
            this.btnShowWizard.TabIndex = 3;
            this.btnShowWizard.Text = "Create a Wizard";
            this.btnShowWizard.UseVisualStyleBackColor = true;
            this.btnShowWizard.Click += new System.EventHandler(this.btnShowWizard_Click);
            // 
            // btnShowBeasts
            // 
            this.btnShowBeasts.Location = new System.Drawing.Point(748, 177);
            this.btnShowBeasts.Name = "btnShowBeasts";
            this.btnShowBeasts.Size = new System.Drawing.Size(136, 63);
            this.btnShowBeasts.TabIndex = 3;
            this.btnShowBeasts.Text = "Create a Beast";
            this.btnShowBeasts.UseVisualStyleBackColor = true;
            this.btnShowBeasts.Click += new System.EventHandler(this.btnShowBeasts_Click);
            // 
            // btnThiefShow
            // 
            this.btnThiefShow.Location = new System.Drawing.Point(748, 12);
            this.btnThiefShow.Name = "btnThiefShow";
            this.btnThiefShow.Size = new System.Drawing.Size(136, 68);
            this.btnThiefShow.TabIndex = 4;
            this.btnThiefShow.Text = "Create a Thief";
            this.btnThiefShow.UseVisualStyleBackColor = true;
            this.btnThiefShow.Click += new System.EventHandler(this.btnThiefShow_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 662);
            this.Controls.Add(this.btnUpDelBeast);
            this.Controls.Add(this.btnUpDelWarrior);
            this.Controls.Add(this.btnDelUpWizard);
            this.Controls.Add(this.btnUpDelThief);
            this.Controls.Add(this.btnThiefShow);
            this.Controls.Add(this.btnShowWarrior);
            this.Controls.Add(this.btnShowWizard);
            this.Controls.Add(this.btnShowBeasts);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lblH3;
        private System.Windows.Forms.Label lblN3;
        private System.Windows.Forms.Label lblT3;
        private System.Windows.Forms.Label lblR3;
        private System.Windows.Forms.Label lblA3;
        private System.Windows.Forms.Label lblM3;
        private System.Windows.Forms.Label lblH2;
        private System.Windows.Forms.Label lblN2;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label lblR2;
        private System.Windows.Forms.Label lblA2;
        private System.Windows.Forms.Label lblM2;
        private System.Windows.Forms.Label lblH1;
        private System.Windows.Forms.Label lblN1;
        private System.Windows.Forms.Label bl;
        private System.Windows.Forms.Label lblR1;
        private System.Windows.Forms.Label lblA1;
        private System.Windows.Forms.Label lblM1;
        private System.Windows.Forms.Label lblT2;
        private System.Windows.Forms.Label lblT1;
        private System.Windows.Forms.Label lblWiH3;
        private System.Windows.Forms.Label lblWiN3;
        private System.Windows.Forms.Label lblWiT3;
        private System.Windows.Forms.Label lblWiS3;
        private System.Windows.Forms.Label lblWiA13;
        private System.Windows.Forms.Label lblWiM3;
        private System.Windows.Forms.Label lblWiH2;
        private System.Windows.Forms.Label lblWiN2;
        private System.Windows.Forms.Label lblWiT2;
        private System.Windows.Forms.Label lblWiS2;
        private System.Windows.Forms.Label lblWiA2;
        private System.Windows.Forms.Label lblWiM2;
        private System.Windows.Forms.Label lblWiH1;
        private System.Windows.Forms.Label lblWiN1;
        private System.Windows.Forms.Label lblWiT1;
        private System.Windows.Forms.Label lblWiS1;
        private System.Windows.Forms.Label lblWiA1;
        private System.Windows.Forms.Label lblWiM1;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblWiA3;
        private System.Windows.Forms.Label lblThiH3;
        private System.Windows.Forms.Label lblThiN3;
        private System.Windows.Forms.Label lblThiT3;
        private System.Windows.Forms.Label lblThiS3;
        private System.Windows.Forms.Label lblThiA3;
        private System.Windows.Forms.Label lblThiM3;
        private System.Windows.Forms.Label lblThiH2;
        private System.Windows.Forms.Label lblThiN2;
        private System.Windows.Forms.Label lblThiT2;
        private System.Windows.Forms.Label lblThiS2;
        private System.Windows.Forms.Label lblThiA2;
        private System.Windows.Forms.Label lblThiM2;
        private System.Windows.Forms.Label lblThiH1;
        private System.Windows.Forms.Label lblThiN1;
        private System.Windows.Forms.Label lblThiT1;
        private System.Windows.Forms.Label lblThiS1;
        private System.Windows.Forms.Label lblThiA1;
        private System.Windows.Forms.Label lblThiM1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblWaH3;
        private System.Windows.Forms.Label lblWaN3;
        private System.Windows.Forms.Label lblWaT3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblWaA3;
        private System.Windows.Forms.Label lblWaM3;
        private System.Windows.Forms.Label lblWaH2;
        private System.Windows.Forms.Label lblWaN2;
        private System.Windows.Forms.Label lblWaT2;
        private System.Windows.Forms.Label lblWaS2;
        private System.Windows.Forms.Label lblWaA2;
        private System.Windows.Forms.Label lblWaM2;
        private System.Windows.Forms.Label lblWaH1;
        private System.Windows.Forms.Label lblWaN1;
        private System.Windows.Forms.Label lblWaT1;
        private System.Windows.Forms.Label lblWaS1;
        private System.Windows.Forms.Label lblWaA1;
        private System.Windows.Forms.Label lblWaM1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblWaS3;
        private System.Windows.Forms.Button btnUpDelThief;
        private System.Windows.Forms.Button btnDelUpWizard;
        private System.Windows.Forms.Button btnUpDelWarrior;
        private System.Windows.Forms.Button btnUpDelBeast;
        private System.Windows.Forms.Button btnShowWarrior;
        private System.Windows.Forms.Button btnShowWizard;
        private System.Windows.Forms.Button btnShowBeasts;
        private System.Windows.Forms.Button btnThiefShow;
    }
}