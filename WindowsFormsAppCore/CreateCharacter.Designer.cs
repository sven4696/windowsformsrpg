﻿namespace WindowsFormsAppCore
{
    partial class CreateCharacter
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEnter = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.lblShowText = new System.Windows.Forms.Label();
            this.lbCharData = new System.Windows.Forms.ListBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btndb = new System.Windows.Forms.Button();
            this.btnshowCharacter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblEnter
            // 
            this.lblEnter.AutoSize = true;
            this.lblEnter.Location = new System.Drawing.Point(12, 136);
            this.lblEnter.Name = "lblEnter";
            this.lblEnter.Size = new System.Drawing.Size(140, 20);
            this.lblEnter.TabIndex = 1;
            this.lblEnter.Text = "Choose a Character:";
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(158, 167);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(119, 53);
            this.btnShow.TabIndex = 3;
            this.btnShow.Text = "Preview Character";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblShowText
            // 
            this.lblShowText.AutoSize = true;
            this.lblShowText.Location = new System.Drawing.Point(-191, -49);
            this.lblShowText.Name = "lblShowText";
            this.lblShowText.Size = new System.Drawing.Size(0, 20);
            this.lblShowText.TabIndex = 5;
            // 
            // lbCharData
            // 
            this.lbCharData.FormattingEnabled = true;
            this.lbCharData.ItemHeight = 20;
            this.lbCharData.Location = new System.Drawing.Point(419, 76);
            this.lbCharData.Name = "lbCharData";
            this.lbCharData.Size = new System.Drawing.Size(105, 144);
            this.lbCharData.TabIndex = 6;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(158, 133);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(119, 28);
            this.comboBox1.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Enter a Name: ";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(158, 91);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(119, 27);
            this.tbName.TabIndex = 9;
            // 
            // btndb
            // 
            this.btndb.Location = new System.Drawing.Point(534, 76);
            this.btndb.Name = "btndb";
            this.btndb.Size = new System.Drawing.Size(124, 64);
            this.btndb.TabIndex = 10;
            this.btndb.Text = "Insert into database";
            this.btndb.UseVisualStyleBackColor = true;
            this.btndb.Click += new System.EventHandler(this.btndb_Click);
            // 
            // btnshowCharacter
            // 
            this.btnshowCharacter.Location = new System.Drawing.Point(534, 152);
            this.btnshowCharacter.Name = "btnshowCharacter";
            this.btnshowCharacter.Size = new System.Drawing.Size(124, 68);
            this.btnshowCharacter.TabIndex = 12;
            this.btnshowCharacter.Text = "Show Characters";
            this.btnshowCharacter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnshowCharacter.UseVisualStyleBackColor = true;
            this.btnshowCharacter.Click += new System.EventHandler(this.btnshowCharacter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(12, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(297, 46);
            this.label2.TabIndex = 13;
            this.label2.Text = "Create a Character";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(366, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Type: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(359, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(353, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Health: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(360, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 20);
            this.label6.TabIndex = 17;
            this.label6.Text = "Mana: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(359, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 20);
            this.label7.TabIndex = 18;
            this.label7.Text = "Armor:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(306, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 20);
            this.label8.TabIndex = 19;
            this.label8.Text = "Special Ability:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 247);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnshowCharacter);
            this.Controls.Add(this.btndb);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lbCharData);
            this.Controls.Add(this.lblShowText);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.lblEnter);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblEnter;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label lblShowText;
        private System.Windows.Forms.ListBox lbCharData;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btndb;
        private System.Windows.Forms.Button btnshowCharacter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

