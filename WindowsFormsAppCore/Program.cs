using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppCore
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new CreateCharacter());

            // Build connection string (CHANGE TO YOUR INSTANCE)
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            // Connect to the SQLServer instance
            Console.Write("Connecting to SQL Server ... ");

            try
            {
                using(SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    Console.WriteLine("Done. \n");

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sql = "SELECT * FROM Beast";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine($"{reader.GetName(0)} \t {reader.GetName(1)}");
                            Console.WriteLine("--------------------------------");

                            while (reader.Read())
                            {
                                Console.WriteLine($"{reader.GetInt32(0)} \t {reader.GetString(1)}");
                            }

                            Console.WriteLine("----------------------------------");

                            Console.WriteLine(reader.FieldCount);
                            Console.WriteLine(reader.IsClosed);
                            Console.WriteLine(reader.VisibleFieldCount);
                            Console.WriteLine(reader.GetName(2));
                            Console.WriteLine(reader.GetDataTypeName(0));


                        }


                    }




                }
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
