﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using RPGCharacters;

namespace WindowsFormsAppCore
{
    public partial class CreateCharacter : Form
    {
 
        public CreateCharacter()
        {
            InitializeComponent();
        }

        // Adding items to the dropdown box
        private void Form1_Load(object sender, EventArgs e) 
        {
          
            comboBox1.Items.Add("Wizard");
            comboBox1.Items.Add("Thief");
            comboBox1.Items.Add("Warrior");
            comboBox1.Items.Add("Beast");

        }

        // Show the selcted item and its character attributes
        private void btnShow_Click(object sender, EventArgs e)
        {
            string item = "";
            try
            {
                item = comboBox1.SelectedItem.ToString();

            } catch (Exception ex)
            {
                MessageBox.Show("please enter a name");
            }
            string name = tbName.Text;


            switch (item)
            {
                case "Wizard":
                    
                    lbCharData.Items.Clear();
                    List<string> li = new List<string>() { "fire", "ice" };
                    Wizard wizard = new Wizard(li, name, 200, 200, 2);

                    lbCharData.Items.Add("Wizard");
                    lbCharData.Items.Add($"{wizard.Name}");
                    lbCharData.Items.Add( wizard.HP);
                    lbCharData.Items.Add( + wizard.Mana);
                    lbCharData.Items.Add(wizard.ArmorRating);
                    lbCharData.Items.Add($"{li[0]} {li[1]}");
                    break;

                case "Thief":
                    lbCharData.Items.Clear();
                    Thief thief = new Thief(10, name, 180, 300,3);

                    lbCharData.Items.Add("Thief");
                    lbCharData.Items.Add($"{thief.Name}");
                    lbCharData.Items.Add( thief.HP);
                    lbCharData.Items.Add(thief.Mana);
                    lbCharData.Items.Add(thief.ArmorRating);
                    lbCharData.Items.Add(thief.StealthModeDuration);
                    break;

                case "Warrior":
                    lbCharData.Items.Clear();
                    Warrior warrior = new Warrior(false, name, 300, 400, 6);

                    lbCharData.Items.Add("Warrior");
                    lbCharData.Items.Add($"{warrior.Name}");
                    lbCharData.Items.Add(warrior.HP);
                    lbCharData.Items.Add(warrior.Mana);
                    lbCharData.Items.Add( warrior.ArmorRating);
                    lbCharData.Items.Add("True");

                    break;

                case "Beast":
                    lbCharData.Items.Clear();
                    Beast beast = new Beast(0, name, 350, 400, 3);

                    lbCharData.Items.Add("Beast");
                    lbCharData.Items.Add($"{beast.Name}");
                    lbCharData.Items.Add(beast.HP);
                    lbCharData.Items.Add( beast.Mana);
                    lbCharData.Items.Add(beast.ArmorRating);
                    lbCharData.Items.Add(beast.RageLevel);
                    break;

                default:
                    MessageBox.Show("Please select a character!");
                    break;
            }

        }

        // Inserting the selcted data into the desired database
        private void btndb_Click(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7267\\SQLEXPRESS";
            builder.InitialCatalog = "RPGdb";
            builder.IntegratedSecurity = true;

            string type = lbCharData.Items[0].ToString();
            string name = "";
            int hp = 0;
            int mana = 0;
            int armor = 0;
            int rage = 0;
            string spells = "";
            string shield = "";
            double stealth = 0;


            string sql = $"INSERT INTO {type} (CharacterType, Name, HP, Mana, ArmorRating, RageMode) VALUES (@type, @name, @hp, @mana, @armor, @rage)";
            string sql2 = $"INSERT INTO {type} (CharacterType, Name, HP, Mana, ArmorRating, StealthMode) VALUES (@type, @name, @hp, @mana, @armor, @stealth)";
            string sql3 = $"INSERT INTO {type} (CharacterType, Name, HP, Mana, ArmorRating, EquiptedShield) VALUES (@type, @name, @hp, @mana, @armor, @shield)";
            string sql4 = $"INSERT INTO {type} (CharacterType, Name, HP, Mana, ArmorRating, Spells) VALUES (@type, @name, @hp, @mana, @armor, @spells)";

            switch (type)
            {
                

                case ("Beast"):
                    name = tbName.Text;
                     hp = (int)lbCharData.Items[2];
                     mana = (int)lbCharData.Items[3];
                     armor = (int)lbCharData.Items[4];
                     rage = (int)lbCharData.Items[5];

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@type", type);
                            command.Parameters.AddWithValue("@name", name);
                            command.Parameters.AddWithValue("@hp", hp);
                            command.Parameters.AddWithValue("@mana", mana);
                            command.Parameters.AddWithValue("@armor", armor);
                            command.Parameters.AddWithValue("@rage", rage);

                            command.ExecuteNonQuery();

                            lbCharData.Items.Clear();
                        }
                    }
                    break;

                case ("Wizard"):
                    name = tbName.Text;
                     hp = (int)lbCharData.Items[2];
                     mana = (int)lbCharData.Items[3];
                     armor = (int)lbCharData.Items[4];
                     spells = lbCharData.Items[5].ToString();

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql4, connection))
                        {
                            command.Parameters.AddWithValue("@type", type);
                            command.Parameters.AddWithValue("@name", name);
                            command.Parameters.AddWithValue("@hp", hp);
                            command.Parameters.AddWithValue("@mana", mana);
                            command.Parameters.AddWithValue("@armor", armor);
                            command.Parameters.AddWithValue("@spells", spells);

                            command.ExecuteNonQuery();

                            lbCharData.Items.Clear();

                        }
                    }
                    break;

                case ("Warrior"):
                     name = tbName.Text;
                     hp = (int)lbCharData.Items[2];
                     mana = (int)lbCharData.Items[3];
                     armor = (int)lbCharData.Items[4];
                     shield = lbCharData.Items[5].ToString();

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql3, connection))
                        {
                            command.Parameters.AddWithValue("@type", type);
                            command.Parameters.AddWithValue("@name", name);
                            command.Parameters.AddWithValue("@hp", hp);
                            command.Parameters.AddWithValue("@mana", mana);
                            command.Parameters.AddWithValue("@armor", armor);
                            command.Parameters.AddWithValue("@shield", shield);

                            command.ExecuteNonQuery();

                            lbCharData.Items.Clear();

                        }
                    }
                    break;

                case ("Thief"):
                     name = tbName.Text;
                     hp = (int)lbCharData.Items[2];
                     mana = (int)lbCharData.Items[3];
                     armor = (int)lbCharData.Items[4];
                     stealth = (double)lbCharData.Items[5];

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();

                        Console.WriteLine("Done. \n");

                        using (SqlCommand command = new SqlCommand(sql2, connection))
                        {
                            command.Parameters.AddWithValue("@type", type);
                            command.Parameters.AddWithValue("@name", name);
                            command.Parameters.AddWithValue("@hp", hp);
                            command.Parameters.AddWithValue("@mana", mana);
                            command.Parameters.AddWithValue("@armor", armor);
                            command.Parameters.AddWithValue("@stealth", stealth);

                            command.ExecuteNonQuery();

                            lbCharData.Items.Clear();

                        }
                    }
                    break;
            }
            
        }

        // Redirect to anohter form where there are some characters on display
        private void btnshowCharacter_Click(object sender, EventArgs e)
        {
            this.Hide();
            ListAllCharacters form2 = new ListAllCharacters();
            form2.ShowDialog();
        }
    }
}
